# gitlab-runner-lfs-issue

This is a test repository which holds some LFS files and a simple Gitlab CI to validate whether gitlab-runners can be run locally with a docker executor when the repo has LFS enabled.

Clone the repo:

```bash
git clone git@gitlab.com:rcrdogvra/gitlab-runner-lfs-issue.git
```

Run the gitlab-runner locally on the `validate` stage

```bash
gitlab-runner exec docker validate
```

